# !!! Changeme !!!

Before using this repo, find and replace `projectname` and `projectdescription`
 and edit `doc/Requirements.md`.

---

# projectname: projectdescription

All documentations are in [/doc](./doc/) folder:
- [App requirements](./doc/Requirements.md)
- [How to run locally (develop)](./doc/Development.md)
- [Some Technical Details](./doc/TechDetails.md)
- [3rd Party Links/Resources Used](./doc/Links.md)

Application is running live at: https://projectname.herokuapp.com/
