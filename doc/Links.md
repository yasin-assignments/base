# 3rd Party Links/Resources Used

- For skel/scaffolding: [microsoft/TypeScript-Node-Starter][1]
- Node.js .gitignore: [github/gitignore][2]
- [Node-TS-Docker][3]

[1]: https://github.com/microsoft/TypeScript-Node-Starter/tree/master/src
[2]: https://github.com/github/gitignore/blob/master/Node.gitignore
[3]: https://github.com/microsoft/TypeScript-Node-Starter/pull/263/files
