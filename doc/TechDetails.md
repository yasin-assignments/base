# Tech Details

## Used Tech Stack
- Node.js (>= 12)
- Express.JS
- NPM (normally I would prefer Yarn)
- Docker + Docker Compose
- TypeScript
- Eslint + Eslint-TS + Prettier
- Nodemon

## Execution flow

API server and application loads these in this order:
- package.json
- src/api/server
- src/api/app
- src/api/controller
- src/api/service
- src/lib/...
