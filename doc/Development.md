# Development

First prepare your local folder:

```sh
mkdir -p ~/gitrepos
cd ~/gitrepos
git clone https://github.com/yasinaydinnet/projectname.git
cd projectname
npm i
```

Then follow one of the scenarios below inside that folder:

## Running Manually
```sh
npm run build
npm start
```

## Via Watch
```sh
npm run watch
```

## Via Docker Compose
```sh
docker-compose up --build
```

## Other Optinos

Please see `package.json`'s scripts property.
