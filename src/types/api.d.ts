type ApiResponse = string;

interface ApiResponseBody {
  status: string;
  statusCode: number;
  error: string;
  data: any;
}
