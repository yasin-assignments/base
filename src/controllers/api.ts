import { testService } from "../services/test";
import { sendApiResponse } from "../util/apiResponse";
import { Request, Response } from "express";

export const testAction = async (
  req: Request,
  res: Response
): Promise<void> => {
  try {
    const data: ApiResponse = await testService();

    sendApiResponse(res, 200, null, data);
  } catch (e) {
    sendApiResponse(res, 400, e.message);
  }
};
