import express from "express";
import * as apiController from "./controllers/api";

const app = express();

app.use("/", express.static("./public"));
app.get("/api/test", apiController.testAction);

export default app;
