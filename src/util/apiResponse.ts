import { Response } from "express";

export const sendApiResponse = (
  res: Response,
  statusCode: number,
  error?: string,
  data?: ApiResponse
): void => {
  const isOk = statusCode === 200;

  res.type("application/json");
  res.header("Content-Type", "application/json");

  const body: ApiResponseBody = {
    status: isOk ? "ok" : "error",
    statusCode,
    error,
    data,
  };

  res.status(statusCode).send(JSON.stringify(body));
};
